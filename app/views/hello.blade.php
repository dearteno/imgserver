<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.1/css/bootstrap-combined.min.css" rel="stylesheet">
		<style>
			table form { margin-bottom: 0; }
			form ul { margin-left: 0; list-style: none; }
			.error { color: red; font-style: italic; }
			body { padding-top: 20px; }
		</style>
	</head>
<body>
<h4>Image Service</h4>
<div class="well">
{{ Form::open(array('url' => 'images/upload','files' => true,'class'=>'form-horizontal','method' => 'post')) }}

	     <div class="control-group" for="id" >
            {{ Form::label('id', 'ID:', array('class' => 'control-label')) }}
             <div class="controls">
            {{ Form::text('id',null,array('class'=>'input-xlarge')) }}
            </div>
        </div>

         <div class="control-group" for="id" >
            {{ Form::label('image', 'Image:', array('class' => 'control-label')) }}
             <div class="controls">
            {{ Form::file('image',null,array('class'=>'input-xlarge')) }}
            </div>
        </div>
            
        <div class="form-actions"> 
			{{ Form::submit('Submit',null,array('class'=>'btn btn-primary')) }}
	 	</div>
{{ Form::close() }}
<ul>
<li>thumbs (200x200) : /images/thumbs/uploads/images/filename.jpg </li>
<li>gallery (800xAuto) : /images/gallery/uploads/images/filename.jpg</li>
<li>preview (800x600) : /images/preview/uploads/images/filename.jpg </li>
<li>full (no resize) : /images/full/uploads/images/filename.jpg</li>
<ul>
</div>
</body>
</html>