<?php

class ImageController extends BaseController {

	/*
	|--------------------------------------------------------------------------
	| Default Home Controller
	|--------------------------------------------------------------------------
	|
	| You may wish to use controllers instead of, or in addition to, Closure
	| based routes. That's great! Here is an example controller method to
	| get you started. To route to this controller, just add the route:
	|
	|	Route::get('/', 'HomeController@showWelcome');
	|
	*/

	public function index()
	{
		return View::make('hello');
	}

	public function upload()
	{
 
 		if (Input::hasFile('image'))
		{
		
		$file = Input::file('image') ;
		$extension = $file->getClientOriginalExtension();
		$destinationPath = public_path() . '/uploads/images/';
		$filename = date('dmyhis') . str_pad(rand(0, 9999) , 4, '0', STR_PAD_LEFT) . '.'.$extension;
		$result = Image::make($file->getRealPath())->save($destinationPath.$filename);
		if ($result) {
			$array = array(
			'full' => '/images/full/uploads/images/'.$filename,
			'thumbs' => '/images/thumbs/uploads/images/'.$filename,
			'gallery' => '/images/gallery/uploads/images/'.$filename,
			'preview' => '/images/preview/uploads/images/'.$filename,
			);
		};


		echo '<pre>'.stripslashes(json_encode($array)).'</pre>';   
	 
		} 

	}
}